import React from 'react';
import PropTypes from 'prop-types';
import classNames from "classnames";
import Input from '../../Form/Input';
import Checkbox from '../../Form/Checkbox';
import Text from '../../Text';
import styles from '../DetailsForm/DetailsForm.module.scss';

const CryptoForm = (props) => {
    const {
        crypto,
        isDisabled,
        register,
        onEdit,
        onSubmit,
        handleSubmit,
        reff: {formRef, btnRef}
    } = props;

    return (
        <div ref={formRef}>
            <div className={styles.title}>
                <Text text="Crypto" />
                <img
                    className={styles.pen}
                    onClick={onEdit}
                    src="/img/pen.png"
                    alt=""
                />
            </div>
            <form
                noValidate
                onSubmit={handleSubmit(onSubmit)}
                className={styles.form}
            >
                <Input
                    label="btc"
                    className={classNames(isDisabled && styles.editable)}
                    name="btc"
                    link={register}
                    defaultValue={crypto && crypto.btc}
                    active={crypto && crypto.active}
                    readOnly={!isDisabled}
                    component={Checkbox}
                />
                <Input
                    label="eth"
                    className={classNames(isDisabled && styles.editable)}
                    name="eth"
                    link={register}
                    defaultValue={crypto && crypto.eth}
                    active={crypto && crypto.active}
                    readOnly={!isDisabled}
                    component={Checkbox}
                />
                <Input
                    label="spr"
                    className={classNames(isDisabled && styles.editable)}
                    name="spr"
                    link={register}
                    defaultValue={crypto && crypto.spr}
                    active={crypto && crypto.active}
                    readOnly={!isDisabled}
                    component={Checkbox}
                />

                <button
                    className={styles.btn}
                    ref={btnRef}
                    type="submit"
                >
                    send
                </button>
            </form>
        </div>
    )
};

CryptoForm.defaultProps = {
    crypto: {}
};

CryptoForm.propTypes = {
    crypto: PropTypes.shape({
        btc: PropTypes.string,
        eth: PropTypes.string,
        spr: PropTypes.string,
        active: PropTypes.string
    }),
    isDisabled: PropTypes.bool.isRequired,
    register: PropTypes.func.isRequired,
    onEdit: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    reff: PropTypes.objectOf(PropTypes.object)
};

export default CryptoForm;