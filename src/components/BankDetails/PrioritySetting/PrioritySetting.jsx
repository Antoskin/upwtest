import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './PrioritySetting.scss';

const PrioritySetting = ({priority, handleClick, isDisabled}) => (
    <div className="priority">
        <button
            onClick={handleClick('crypto')}
            className={
                classNames('priority__btn',
                    priority === 'crypto' ?
                        'active': null)}
        >
            crypto
        </button>
        <button
            onClick={handleClick('bank')}
            className={
                classNames('priority__btn',
                    priority === 'bank' ?
                        'active' : null )}
            disabled={isDisabled}
        >
            bank
        </button>
    </div>
);

PrioritySetting.defaultProps = {
    priority: 'crypto'
};

PrioritySetting.propTypes = {
    priority: PropTypes.string,
    handleClick: PropTypes.func.isRequired,
    isDisabled: PropTypes.bool.isRequired
};

export default PrioritySetting;