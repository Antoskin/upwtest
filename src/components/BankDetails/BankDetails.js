import React, {memo, useState} from 'react';
import get from 'lodash/get';
import DetailsForm from './DetailsForm';
import CryptoForm from './CryptoForm';
import PrioritySetting from './PrioritySetting';
import FieldWrapper from '../Form/FieldWrapper';
import {useHttp} from '../../hooks/http.hook'
import {useForm} from '../../hooks/form.hook';
import {FormContext} from '../../context';

function BankDetails() {
    const [bankDetails, setBankDetails] = useState({});
    const {loading} = useHttp();
    const {
        handlePriority,
        handleCrypto,
        onSubmit
    } = useForm(setBankDetails);

    const isEmptyField = get(bankDetails, 'bank', {});
    const isFilled = Object.keys(isEmptyField).filter(field => isEmptyField[field] === '');

    if (loading) {
        return 'loading...'
    }

    return (
        <FormContext.Provider value={{
            handleCrypto
        }}>
            <FieldWrapper
                bank={bankDetails.bank}
                component={DetailsForm}
                onSubmit={onSubmit}
            />

            <FieldWrapper
                crypto={bankDetails.crypto}
                component={CryptoForm}
                onSubmit={onSubmit}
            />
            <PrioritySetting
                priority={bankDetails.priority}
                handleClick={handlePriority}
                isDisabled={!!isFilled.length}
            />
        </FormContext.Provider>
    );
}

export default memo(BankDetails);