import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Input from '../../Form/Input';
import Text from '../../Text';
import styles from './DetailsForm.module.scss';

const DetailsForm = (props) => {
    const {
        bank,
        register,
        isDisabled,
        onEdit,
        onSubmit,
        handleSubmit,
        reff: {formRef, btnRef}
    } = props;

    return (
        <div ref={formRef}>
            <div className={styles.title}>
                <Text text="Bank transfer" />
                <img
                    className={styles.pen}
                    onClick={onEdit}
                    src="/img/pen.png"
                    alt=""
                />
            </div>
            <form
                onSubmit={handleSubmit(onSubmit)}
                className={styles.form}
            >
                <Input
                    label="Bank name"
                    name="bankName"
                    link={register}
                    className={classNames(isDisabled && styles.editable)}
                    defaultValue={bank ? bank.bankName : ''}
                    readOnly={!isDisabled}
                />
                <Input
                    label="Account number"
                    name="accountNumber"
                    link={register}
                    className={classNames(isDisabled && styles.editable)}
                    defaultValue={bank ? bank.accountNumber : ''}
                    readOnly={!isDisabled}
                />
                <Input
                    label="SWIFT"
                    name="swift"
                    link={register}
                    className={classNames(isDisabled && styles.editable)}
                    defaultValue={bank ? bank.swift : ''}
                    readOnly={!isDisabled}
                />
                <Input
                    label="Bank ID"
                    name="bankId"
                    link={register}
                    className={classNames(isDisabled && styles.editable)}
                    defaultValue={bank ? bank.bankId : ''}
                    readOnly={!isDisabled}
                />
                <Input
                    label="Bank Address"
                    name="bankAddress"
                    link={register}
                    className={classNames(isDisabled && styles.editable)}
                    defaultValue={bank ? bank.bankAddress : ''}
                    readOnly={!isDisabled}
                />
                <button className={styles.btn} type="submit" ref={btnRef}>
                    send
                </button>
            </form>
        </div>
    );
};

DetailsForm.defaultProps = {
    bank: {}
};

DetailsForm.propTypes = {
    bank: PropTypes.shape({
        bankName: PropTypes.string,
        accountNumber: PropTypes.string,
        swift: PropTypes.string,
        bankId: PropTypes.string,
        bankAddress: PropTypes.string,
        country: PropTypes.string
    }),
    isDisabled: PropTypes.bool.isRequired,
    register: PropTypes.func.isRequired,
    onEdit: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    reff: PropTypes.objectOf(PropTypes.object)
};

export default DetailsForm;