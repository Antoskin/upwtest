import React from 'react';
import PropTypes from 'prop-types';
import styles from './Text.module.css';

const Text = ({text}) => (
    <div className={styles.p}>
        {text}
    </div>
);

Text.propTypes = {
    text: PropTypes.string.isRequired
};

export default Text;