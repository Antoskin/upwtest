import React from 'react';
import PropTypes from 'prop-types';
import Text from '../../Text';
import styles from './Input.module.scss';

function Input({ type, label, name, link, className, component: Checkbox, active, ...args }) {
    return (
        <div className={styles.field}>
            {!!Checkbox && <Checkbox name={name} active={active} {...args} />}
            <Text text={label} />
            <input
                type={type}
                name={name}
                ref={link}
                className={`${styles.input} ${className}`}
                {...args}
            />
        </div>
    );
}

Input.defaultProps =  {
    type: 'text',
    className: null,
    component: null,
};

Input.propTypes = {
    type: PropTypes.string,
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    link: PropTypes.func.isRequired,
    className: PropTypes.string,
    component: PropTypes.func
};

export default Input;