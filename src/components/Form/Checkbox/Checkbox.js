import React, {useContext} from 'react';
import {FormContext} from '../../../context';
import './Checkbox.scss';

function Checkbox({name, active, defaultValue: isDisabled}) {
    const {handleCrypto} = useContext(FormContext);

    return (
        <label className="container">
            <input
                type="checkbox"
                checked={name === active}
                onChange={handleCrypto(name)}
                disabled={!isDisabled}
            />
            <span className="checkmark"></span>
        </label>
    );
}

export default Checkbox;
