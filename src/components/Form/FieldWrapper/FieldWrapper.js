import React, {useEffect, useRef, useState} from 'react';
import PropTypes from 'prop-types';
import {useForm} from 'react-hook-form';
import {useOutside} from '../../../hooks/click.out.hook';

function FieldWrapper({component: Component, onSubmit, ...args}) {
    const [edit, setEdit] = useState(false);
    const {register, handleSubmit} = useForm();
    const isInitialMount = useRef(true);
    const formRef = useRef(null);
    const btnRef = useRef(null);
    const {out, setOut} = useOutside(formRef);

    useEffect(() => {
        if (isInitialMount.current) {
            isInitialMount.current = false;
        } else {
            if (out === false) {
                setEdit(false);
                btnRef.current.click();
            }
        }
        // eslint-disable-next-line
    }, [out]);

    const editHandler = () => {
        setEdit(!edit);
        setOut(true);
    };

    return (
        <Component
            {...args}
            isDisabled={edit}
            register={register}
            onEdit={() => editHandler()}
            onSubmit={(e) => onSubmit(e)}
            handleSubmit={handleSubmit}
            reff={{
                formRef,
                btnRef
            }}
        />
    );
}

FieldWrapper.defaultProps = {
    handleCrypto: null
};

FieldWrapper.propTypes = {
    component: PropTypes.func.isRequired,
    args: PropTypes.object,
};

export default FieldWrapper;