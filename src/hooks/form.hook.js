import {useEffect} from 'react';
import {useHttp} from './http.hook';
import {restUrl} from '../constants';

export const useForm = (setBankDetails) => {
    const {request} = useHttp();

    useEffect(() => {
        async function fetchData() {
            const res = await request(restUrl);
            setBankDetails(res.data);
        }

        fetchData();
        // eslint-disable-next-line
    }, [request]);

    const handlePriority = priority => async () => {
        const res = await request(restUrl, 'PATCH', {priority} );
        setBankDetails(res.data);
    };

    const handleCrypto = cur => async () => {
        const crypto = { active: cur };

        const res = await request(restUrl, 'PATCH', {crypto});
        setBankDetails(res.data)
    };

    const onSubmit = async (fields) => {
        if (fields.btc) {
            const res = await request(restUrl, 'PATCH', {crypto: {...fields}});
            return setBankDetails(res.data);
        }

        const res = await request(restUrl, 'PATCH', {bank: {...fields}});
        setBankDetails(res.data);
    };

    return {
        handlePriority,
        handleCrypto,
        onSubmit,
    }
};