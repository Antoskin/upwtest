import {useEffect, useState} from 'react';

export function useOutside(ref) {
  const [out, setOut] = useState(false);
  const clickOut = event => {
    if (ref.current && !ref.current.contains(event.target) && out !== false) {
      setOut(false);
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", clickOut);
    return () => {
      document.removeEventListener("mousedown", clickOut);
    };
  });

  return {out, setOut};
}
