import {useState, useCallback} from 'react';

const jwtToken = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZGU3Y2E3N2NjZjM2ODAwMTQ0YjQ2ZDciLCJpYXQiOjE1ODAyMTk5NzksImV4cCI6MTYxMTMyMzk3OX0.8D52ORsBqmBAzZif_LP-HkCdBQJk_kuv5QZ4oT0nUz8';

export const useHttp = () => {
    const [loading, setLoading] = useState(false);
    const[error, setError] = useState(null);

    const request = useCallback(async (
            url,
            method = 'GET',
            body = null,
            headers = {
                'Authorization': jwtToken
            }
        ) => {
            setLoading(true);

            try {
                if(body) {
                    body = JSON.stringify(body);
                    headers['Content-Type'] = 'application/json';
                    headers['Accept'] = 'application/json';
                }

                const res = await fetch(url, {method, body, headers});
                const data = await res.json();

                if(!res.ok) {
                    throw new Error(data.message || 'something wrong');
                }

                setLoading(false);
                return data
            } catch (e) {
                setLoading(false);
                setError(e.message);

                throw e
            }
        }, []);

    const throwError = () => setError(null);

    return {loading, error, request, throwError}
};
