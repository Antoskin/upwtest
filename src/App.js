import React from 'react';
import BankDetails from './components/BankDetails'

function App() {
  return (
    <div className="App">
      <BankDetails />
    </div>
  );
}

export default App;
